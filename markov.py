import logging
from typing import cast

import markovify
from _typeshed import StrPath
from markovify import Text


class MarkovBlabberer:
    def __init__(self, filepath: StrPath):
        self.logger = logging.getLogger("markov")
        self.filepath = filepath

        with open(filepath) as f:
            text = f.read()
            self.markov: Text = markovify.NewlineText(text.lower())
            self.logger.info("Sentence of the day: " + self.make_sentence())

    def make_sentence(self, tries: int = 100):
        return self.markov.make_sentence(tries=tries) or "???"

    def add_to_corpus(self, text: str):
        text = text.lower()
        new_sentence = markovify.NewlineText(text)
        self.markov = cast(Text, markovify.combine([self.markov, new_sentence]))
        with open(self.filepath, "a") as f:
            f.write(text + "\n")
