from _typeshed import StrPath
from datetime import datetime
import os
import errno


def mkdir_p(path: StrPath) -> None:
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def datestr(date: datetime) -> str:
    return date.strftime("%Y-%m-%d@%H%M")
