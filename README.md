#DeLojza

## Commands

* /start - blonk :^)
* /retag - correct tags on last downloaded audio (format: /retag artist - title)
* /delete - undo last download
* /stats - show statistics about tags
* /orphans - show orphaned (single file only) tags
* /orphans_full - show orphaned tags, along with filenames in them
* /version - show delojza and youtube-dl version