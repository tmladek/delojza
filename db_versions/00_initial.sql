CREATE TABLE chats
(
    id        INTEGER PRIMARY KEY NOT NULL,
    protected BOOLEAN             NOT NULL DEFAULT FALSE
);

CREATE TABLE tags
(
    id        INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    tag       TEXT                              UNIQUE NOT NULL,
    protected BOOLEAN                           NOT NULL DEFAULT FALSE
)